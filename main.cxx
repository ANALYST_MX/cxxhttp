#include "stdafx.hxx"

#include "main.hxx"

const std::string HTTP = "HTTP/1.1 200 OK\r\n"
"Connection: close\r\n"
"Content-type: text/html\r\n"
"\r\n";

char path[256];
char buffer[1024];
int size;

int main(int /*argc*/, char */*argv*/[])
{
	CServer server;
	CFileReader response;

	server.enable();

	if (!server.init(80)) 
	{
		printf("Could not create listening socket!\n");
		return EXIT_FAILURE;
	}

	while (true)
	{
		memset(buffer, '\0', sizeof(buffer));

		while (server.getRequest(buffer, sizeof(buffer))){};

		//GET /index.html HTTP/1.1

		if (strncmp(buffer, "GET ", strlen("GET ")) == 0) 
		{
			char *page = buffer + strlen("GET ");
			char *end = strchr(page, ' ');

			if (end && strncmp(end + 1, "HTTP", strlen("HTTP")) == 0)
				snprintf(path, sizeof(path), "%.*s", static_cast<int>(end - page), page);
			else
				strcpy_s(path, "index.html");
		}
		else 
			strcpy_s(path, "index.html");

		sanitise(path);

		printf("F: %s\n", path);

		if (!response.read(path))
			if (!response.read("404.html"))
				return EXIT_FAILURE;

		size = snprintf(buffer, sizeof(buffer), "%s%.*s", HTTP.c_str(), static_cast<int>(response.getSize()), response.getData().c_str());
		server.sendResponse(buffer, size + 1);

		printf("S: %.*s\n", size, buffer);
	}

	return EXIT_SUCCESS;
}

std::string sanitise(const std::string &str)
{
	return remove_chars(str, "\\/");
}

std::string remove_chars(const std::string &str, const std::string &chars)
{
	std::string res = str;
	res.erase(remove_if(res.begin(), res.end(), [&chars](const char& c) {
		return chars.find(c) != std::string::npos;
	}), res.end());

	return res;
}