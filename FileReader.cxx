#include "stdafx.hxx"

#include <new>

CFileReader::~CFileReader()
{
	delete m_buffer;
	delete m_handle;
}

bool CFileReader::read(const std::string &filename)
{
	auto ok{ true };
	auto err = Error::ERROR_NO;

	fopen_s(&m_handle, filename.c_str(), "rb");
	if (m_handle == nullptr)
		err = Error::ERROR_FOPEN;
	else {
		fseek(m_handle, 0, SEEK_END);
		m_size = ftell(m_handle);

		m_buffer = new (std::nothrow) std::vector<uint8_t>(m_size + 1);
		if (m_buffer == nullptr)
			err = Error::ERROR_MALLOC;
	}

	if (err == Error::ERROR_FOPEN || err == Error::ERROR_MALLOC)
	{
		printf("%s!\n", errors[static_cast<size_t>(err)]);
		return !ok;
	}

	rewind(m_handle);
	fread(m_buffer->data(), m_size, 1, m_handle);

	fclose(m_handle);
	(*m_buffer)[m_size] = '\0';

	return ok;
}

size_t CFileReader::getSize() const
{
	return m_size;
}

std::string CFileReader::getData() const
{
	return std::string(m_buffer->begin(), m_buffer->end());
}