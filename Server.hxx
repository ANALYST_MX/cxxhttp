#pragma once

#include <winsock2.h>

#ifdef _MSC_VER
#pragma comment(lib, "Ws2_32.lib") // Winsock Library file Ws2_32.lib
#endif

class CServer
{
public:
	CServer() = default;
	virtual ~CServer();
	bool enable(); // инициализация WinSock
	static void disable();

	bool init(u_short port);
	inline void close() const;
	inline void closeClient() const;

	bool getRequest(char *, int);
	bool sendResponse(char *, int);
	int errorStatus() const;
protected:
	inline bool create();
	void socketAddress(u_short, u_long);
	bool bindSocket();
	bool startListen();
	static void printError(const char *);
	static void printError(const char *, errno_t);
private:
	SOCKET m_listener;
	SOCKET m_client;
	SOCKADDR_IN m_addressInfo;
	int m_addressInfoSize{};
	int m_errorInfo{};
};

