#pragma once

#include <string>
#include <vector>

class CFileReader
{
public:
	CFileReader() = default;
	virtual ~CFileReader();
	bool read(const std::string &);
	size_t getSize() const;
	std::string getData() const;
protected:
	enum class Error : int8_t {
		ERROR_NO = 0,
		ERROR_FOPEN,
		ERROR_MALLOC,
		ERROR_NR_ITEMS
	};
	char *errors[static_cast<size_t>(Error::ERROR_NR_ITEMS)]{
		"",
		"Could not open file",
		"Could not allocate memory",
	};
private:
	FILE *m_handle{ nullptr };
	size_t m_size{};
	std::vector<uint8_t> *m_buffer;
};

