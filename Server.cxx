#include "stdafx.hxx"

#include <windows.h>
#include <cassert>

CServer::~CServer()
{
	closeClient();
	close();
	disable();
}


bool CServer::enable()
{
	auto ok{ true };
	WSADATA wsaData;
	if (WSAStartup(MAKEWORD(2, 2), &wsaData))
	{
		printError("WSAStartup() failed: %s\n", errno);
		m_errorInfo = WSAGetLastError();
		disable();
		ok = false;
	}

	return ok;
}

void CServer::disable()
{
	WSACleanup();
}

bool CServer::init(u_short port)
{
	bool ok;

	ok = create();
	if (!ok)
	{
		printError("socket error: %s\n", errno);
		disable();
		return ok;
	}

	memset(&m_addressInfo, 0x00, sizeof(m_addressInfo));
	socketAddress(port, INADDR_ANY);

	ok = bindSocket();
	if (!ok)
	{
		printError("binding socket error: %s\n", errno);
		close();
		disable();
		return ok;
	}

	ok = startListen();
	if (!ok)
	{
		printError("listen failed with error: %s\n", errno);
		close();
		disable();
		return ok;
	}

	return ok;
}

bool CServer::startListen()
{
	assert(m_listener != INVALID_SOCKET);
	auto ok{ true };
	if (::listen(m_listener, SOMAXCONN) == SOCKET_ERROR)
	{
		m_errorInfo = WSAGetLastError();
		ok = false;
	}

	return ok;
}

bool CServer::bindSocket()
{
	assert(m_listener != INVALID_SOCKET);
	auto ok{ true };
	if (::bind(m_listener, reinterpret_cast<LPSOCKADDR>(&m_addressInfo), sizeof(struct sockaddr)) == SOCKET_ERROR)
	{
		m_errorInfo = WSAGetLastError();
		ok = false;
	}

	return ok;
}

void CServer::socketAddress(u_short serverPort, u_long serverIp)
{
	m_addressInfo.sin_family = AF_INET;
	m_addressInfo.sin_addr.s_addr = serverIp;
	m_addressInfo.sin_port = htons(serverPort);
}

bool CServer::create()
{
	auto ok{ true };
	struct protoent *ppe;
	ppe = getprotobyname("tcp");
	m_listener = ::socket(AF_INET, SOCK_STREAM, ppe->p_proto);

	if (INVALID_SOCKET == m_listener)
	{
		m_errorInfo = WSAGetLastError();
		ok = false;
	}

	return ok;
}

void CServer::close() const
{
	closesocket(m_listener);
}

void CServer::closeClient() const
{
	closesocket(m_client);
}

int CServer::errorStatus() const
{
	return m_errorInfo;
}

void CServer::printError(const char *message)
{
	printf(message);
}

void CServer::printError(const char *message, errno_t err)
{
	char errstr[CHAR_MAX];
	strerror_s(errstr, err);
	printf(message, errstr);
}

bool CServer::getRequest(char *buffer, int size)
{
	auto ok{ true };

	m_client = ::accept(m_listener, reinterpret_cast<struct sockaddr *>(&m_addressInfo), &m_addressInfoSize);
	if (m_client == INVALID_SOCKET)
	{
		m_errorInfo = WSAGetLastError();
		disable();
		printError("invalid socket\n");
		return !ok;
	}
	auto nbytes = recv(m_client, buffer, size, 0);
	if(nbytes == SOCKET_ERROR)
	{
		m_errorInfo = WSAGetLastError();
		disable();
		printError("recv faild\n");
		return !ok;
	}

	return ok;
}

bool CServer::sendResponse(char *buffer, int size) 
{
	auto ok{true};

	if (send(m_client, buffer, size, 0) == SOCKET_ERROR)
	{
		m_errorInfo = WSAGetLastError();
		disable();
		printError("send error\n");
		ok = false;
	}

	closeClient();

	return ok;
}